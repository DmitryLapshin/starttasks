<?php
    //Версия для строгого сравнения цифр, т. е. не проходят числа вида 1338.
    //Опять сделал и массив, и вывод.
    $answer = array();
    for ($i = 1000; $i < 9999; $i += 2) {
        $digit = $i % 10;
        $N = $i / 10;
        $goodUp = true;
        $goodDown = true;
        while ($N >= 1) {
            if ($N % 10 >= $digit) {
                $goodUp = false;
            }
            if ($N % 10 <= $digit) {
                $goodDown = false;
            }
            if ($goodUp or $goodDown) {
                $digit = $N % 10;
                $N = $N / 10;
            } else {
                continue 2;
            }
        }
        $answer[] = $i;
        echo "$i ";
    }
?>