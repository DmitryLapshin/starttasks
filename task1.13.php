<?php
    //По условию симметричными считаются числа вида 1313.
    $n = 10; $m = 289;
    $number = 1;
    for ($i = 0; $i < $n; $i++) {
        $number *= 2;
    }
    $number += $m;
    //Находим порядок получившегося числа:
    $nCopy = $number; $exp = 0;
    while ($nCopy >= 1) {
        $nCopy /= 10;
        $exp++;
    }
    if ($exp % 2 != 0) {
        $answer = "Число не является симметричным.";
    }
    else {
        $answer = "Число является симметричным.";
        $exp /= 2;
        //Ищем положение цифры, с которой нужно сравнить первую цифру числа:
        $factor = 1;
        for ($i = 0; $i < $exp; $i++) {
            $factor *= 10;
        }
        for ($i = 0; $i < $exp; $i++) {
            if ((($number % ($factor * 10)) / $factor) % 10 != $number % 10) {
                $answer = "Число не является симметричным.";
                break;
            }
            else {
                $number /= 10;
            }
        }
    }
    echo $answer;
?>