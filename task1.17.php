<?php
    $N = 28; $M = 70;
    $NOD = 1;
    if ($N > $M) {
        $lim = $M;
    }
    else {
        $lim = $N;
    }
    for ($i = 2; $i <= $lim; $i++) {
        if ($N % $i == 0 and $M % $i == 0) {
            $NOD = $i;
        }
    }
    echo $NOD;
?>