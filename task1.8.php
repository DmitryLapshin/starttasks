<?php
    //Конкретно для четырёхзначных проще другой алгоритм, но я сделал общий случай.
    function tenPower($i) {
        if ($i === 0) {
            return 1;
        }
        else {
            return 10 * tenPower($i-1);
        }
    }
    function task1_7($N) {
        $M = 0; $answer = true;
        while ($N >= 1) {
            $digit = $N % 10;
            $M += tenPower($digit);
            if (($M / tenPower($digit)) % 10 > 1) {
                $answer = false;
                break;
            }
            else {
                $N /= 10;
            }
        }
        return $answer;
    }
    for ($i = 1000; $i < 10000; $i++) {
        if (task1_7($i)) {
            echo "$i ";
        }
    }
?>