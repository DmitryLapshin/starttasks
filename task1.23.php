<?php
    $N = 494533043996;
    $factor = 1; $M = 0;
    for ($i = 9; $i > 0; $i--) {
        $Ncopy = $N;
        while ($Ncopy >= $i) {
            if ($Ncopy % 10 == $i) {
                $M += $i * $factor;
                $factor *= 10;
            }
            $Ncopy /= 10;
        }
    }
    echo $M;
?>