<?php
    $N = 987654321;
    $M = 0; $Ncopy = $N; $tenPower = 1;
    //Сначала узнаём порядок N:
    while ($Ncopy >= 1) {
        $Ncopy /= 10;
        $tenPower *= 10;
    }
    while ($N >= 1) {
        $digit = $N % 10;
        $tenPower /= 10;
        $M += $digit * $tenPower;
        $N /= 10;
    }
    echo $M;
?>