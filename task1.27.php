<?php
    //Массив используется по условию задачи.
    $m = 5; $n = array(335, 336, 337, 338, 339);
    function isPrime($N) {
        $N2 = $N / 2;
        for ($i = 2; $i <= $N2; $i++) {
            if ($N % $i == 0) {
                return false;
            }
        }
        return true;
    }
    function SumPrime($n) {
        $s = 1;
        $n2 = $n / 2;
        for ($i = 2; $i <= $n2; $i++) {
            if (isPrime($i) and $n % $i == 0) {
                $s += $i;
            }
        }
        //Если число простое, оно тоже является для себя простым делителем:
        if ($s == 1) {
            $s += $n;
        }
        return $s;
    }
    $answer = 0; $ansSum = SumPrime($n[0]);
    for ($i = 1; $i < $m; $i++) {
        $s = SumPrime($n[$i]);
        if ($s > $ansSum) {
            $ansSum = $s;
            $answer = $i;
        }
    }
    echo $n[$answer];
?>