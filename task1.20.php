<?php
    //Сумма целых делителей целого числа всегда равна 0.
    //Предполагаем, что в задаче речь идёт о натуральных числах.
    $N = 124; $M = 349;
    function Sum($number) {
        $s = 1; $N2 = $number / 2;
        for ($i = 2; $i <= $N2; $i++) {
            if ($number % $i == 0) {
                $s += $i;
            }
        }
        return $s;
    }
    $answer = $N; $ansSum = Sum($N);
    for ($i = $N + 1; $i <= $M; $i++) {
        if (Sum($i) > $ansSum) {
            $answer = $i;
            $ansSum = Sum($i);
        }
    }
    echo $answer;
?>