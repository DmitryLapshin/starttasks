<?php
    //Массив используется по условию задачи.
    $m = 5; $n = array(3305, 17, 2, 666, 62734657);
    for ($i = 0; $i < $m; $i++) {
        $factor = 1; $max = 0;
        for ($j = 0; $j < 10; $j++) {
            $nCopy = $n[$i];
            while ($nCopy >= 1) {
                if ($nCopy % 10 == $j) {
                    $max += $j * $factor;
                    $factor *= 10;
                }
                $nCopy /= 10;
            }
        }
        echo "$max ";
    }
?>