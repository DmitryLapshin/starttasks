<?php
    //Массив используется по условию задачи.
    //Считаем, что подходят варианты с вычёркиванием 0 цифр.
    $k = 6; $m = 7; $n = array(330005, 17, 2, 666, 62734657, 555, 999);
    //Функция подсчёта суммы цифр числа
    function Sum($n) {
        $s = 0;
        while ($n >= 1) {
            $s += $n % 10;
            $n /= 10;
        }
        return $s;
    }
    //Функция рекурсивного вычёркивания цифр
    function checkSum($N, $exp) {
        $s = Sum($N);
        global $k;
        if ($s == $k) {
            return true;
        }
        else if ($s < $k) {
            return false;
        }
        else {
            $factor = 1;
            for ($i = 0; $i < $exp; $i++) {
                $factor *= 10;
                $Ncopy = $N % ($factor / 10) + $factor / 10 * (($N / $factor) % $N);
                if (checkSum($Ncopy, $exp - 1)) {
                    return true;
                }
            }
            return false;
        }
    }

    for ($i = 0; $i < $m; $i++) {
        //Находим количество цифр в числе
        $exp = 0; $Ncopy = $n[$i];
        while ($Ncopy >= 1) {
            $exp++;
            $Ncopy /= 10;
        }
        if (checkSum($n[$i], $exp)) {
            $answer = "можно";
        }
        else  {
            $answer = "нельзя";
        }
        echo "$n[$i]: $answer<br>";
    }
?>