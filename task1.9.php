<?php
    $N = 13579;
    $tenPower = 1; $Ncopy = $N;
    //Сначала узнаём порядок N:
    while ($Ncopy >= 1) {
        $Ncopy /= 10;
        $tenPower *= 10;
    }
    if ($N == ($N * $N) % $tenPower) {
        $answer = "Данное число является автоморфным.";
    }
    else {
        $answer = "Данное число не является автоморфным.";
    }
    echo $answer;
?>