<?php
    //Вместо много echo можно собрать результаты в строку, но строки вроде нельзя юзать
    $N = 44307;
    $Ncopy = $N;
    echo "а) ";
    //Определяем порядок двоичной записи
    $i = 1;
    while ($i < $Ncopy) {
        $i *= 2;
    }
    while ($Ncopy > 0) {
        $i /= 2;
        if ($Ncopy >= $i) {
            echo 1;
            $Ncopy -= $i;
        }
        else {
            echo 0;
        }
    }
    echo "<br>б) ";
    //Определяем порядок шестнадцатиричной записи
    $i = 1;
    while ($i < $N) {
        $i *= 16;
    }
    while ($N > 0) {
        $i /= 16;
        $digit = ($N / $i) % 16;
        switch ($digit) {
            case 15:
                echo 'F'; break;
            case 14:
                echo 'E'; break;
            case 13:
                echo 'D'; break;
            case 12:
                echo 'C'; break;
            case 11:
                echo 'B'; break;
            case 10:
                echo 'A'; break;
            default:
                echo $digit;
        }
        $N -= $i * $digit;
    }
?>