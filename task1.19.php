<?php
    $N = 102; $M = 30;
    $NOK = 1;
    if ($N > $M) {
        $min = $M;
    }
    else {
        $min = $N;
    }
    $i = 2;
    //Сначала проходим общие делители N и M
    while ($i <= $min) {
        if ($N % $i == 0 and $M % $i == 0) {
            $NOK *= $i;
            $N /= $i;
            $M /= $i;
            $min /= $i;
        }
        else {
            $i++;
        }
    }
    $NOK = $NOK * $N * $M;
    echo $NOK;
?>