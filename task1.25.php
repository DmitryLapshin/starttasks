<?php
    //Массив используется по условию задачи.
    $m = 5; $n = array(1, 2, 3, 4, 5);
    $nCount = $nSum = 0;
    function Factorial($n) {
        $f = 1;
        if ($n != 0) {
            for ($i = 1; $i <= $n; $i++) {
                $f *= $i;
            }
        }
        return $f;
    }
    for ($i = 0; $i < $m; $i++) {
        $nCopy = $n[$i];
        $sum = 0;
        while ($nCopy >= 1) {
            $sum += Factorial($nCopy % 10);
            $nCopy /= 10;
        }
        if ($sum == $n[$i]) {
            $nCount++;
            $nSum += $n[$i];
        }
    }
    echo "Количество чисел: $nCount<br>Сумма чисел: $nSum";
?>