<?php
    $N = 100000;
    function isPalindrom($N) {
        $Ncopy = $N; $tenPower = 1;
        while ($Ncopy >= 1) {
            $Ncopy /= 10;
            $tenPower *= 10;
        }
        while ($N > 9) {
            $tenPower /= 10;
            if ($N % 10 == ($N / $tenPower) % 10) {
                $N = $N - ($N % 10) - ($N % 10) * $tenPower;
                $N /= 10; $tenPower /= 10;
            }
            else {
                return false;
            }
        }
        return true;
    }
    for ($i = 1; $i < $N; $i++) {
        if (isPalindrom($i) and isPalindrom ($i * $i)) {
            echo "$i ";
        }
    }
?>