<?php
    $N = 2640;
    $i = 2;
    while ($N > 1) {
        if ($N % $i == 0) {
            echo "$i ";
            $N /= $i;
        }
        else {
            $i++;
        }
    }
?>