<?php
    //Не стал делать массив, просто вывожу эти числа.
    for ($i = 1000; $i < 10000; $i++) {
        $N = $i;
        $digit = $N % 10;
        $good = true;
        while ($N >= 1) {
            switch ($digit) {
                case 0: case 2: case 3: case 7:
                    $N /= 10;
                    $digit = $N % 10;
                    break;
                default:
                    $good = false;
                    break 2;
            }
        }
        if ($good) {
            echo "$i ";
        }
    }
?>