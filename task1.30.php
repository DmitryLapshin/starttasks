<?php
    $n = 55;
    function isPrime($N) {
        $N2 = $N / 2;
        for ($i = 2; $i <= $N2; $i++) {
            if ($N % $i == 0) {
                return false;
            }
        }
        return true;
    }
    $nEnd = 2 * $n - 1;
    while (!isPrime($n) and $n < $nEnd) {
        $n++;
    }
    $n2 = $n + 2;
    while ($n2 <= $nEnd) {
        if (isPrime($n2)) {
            if ($n2 - $n == 2) {
                echo "$n и $n2, ";
            }
            $n = $n2;
            $n2++;
        }
        $n2++;
    }
?>